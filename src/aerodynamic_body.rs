use crate::aerodynamic_shape::AerodynamicShape;
use crate::DebugTool;
use gdnative::api::{CollisionShape, Engine, PhysicsDirectBodyState, RigidBody, OS};
use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(RigidBody)]
pub struct AerodynamicBody {
    pub aerodynamic_shapes: Vec<AerodynamicShape>,
    debug_tool: Option<Instance<DebugTool, Shared>>,
}

impl AerodynamicBody {
    fn new(_owner: &RigidBody) -> Self {
        AerodynamicBody {
            aerodynamic_shapes: Vec::new(),
            debug_tool: None,
        }
    }
}

#[methods]
impl AerodynamicBody {
    #[godot]
    fn _enter_tree(&mut self, #[base] owner: &RigidBody) {
        self.aerodynamic_shapes = self.get_aerodynamic_shapes(owner);
        if OS::godot_singleton().is_debug_build() {
            let debug_tool = DebugTool::new_instance();
            let shared = debug_tool.into_shared();
            owner.add_child(&shared, false);
            self.debug_tool = Some(shared)
        }
    }

    #[godot]
    fn _integrate_forces(&self, #[base] owner: &RigidBody, state: Ref<PhysicsDirectBodyState>) {
        for aerodynamic_shape in self.aerodynamic_shapes.iter() {
            aerodynamic_shape.apply_aerodynamics(owner, &state, &self.debug_tool);
        }
    }

    #[godot]
    fn _process(&self, #[base] owner: &RigidBody, _delta: f64) {
        if !Engine::godot_singleton().is_editor_hint() {
            return;
        }

        // Let's debug!
        for aerodynamic_shape in self.aerodynamic_shapes.iter() {
            aerodynamic_shape.draw_debug(owner, &self.debug_tool);
        }
    }
}

impl AerodynamicBody {
    fn get_aerodynamic_shapes(&self, owner: &RigidBody) -> Vec<AerodynamicShape> {
        let children = owner.get_children() as VariantArray;
        children
            .iter()
            .filter_map(|child| child.to_object::<CollisionShape>())
            .map(AerodynamicShape::from_collision_shape)
            .collect()
    }
}
