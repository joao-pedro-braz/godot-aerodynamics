use gdnative::api::{ImmediateGeometry, Mesh, OS};
use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(ImmediateGeometry)]
pub struct DebugTool {
    commands: Vec<(String, Vec<Variant>)>,
}

impl DebugTool {
    fn new(_owner: &ImmediateGeometry) -> Self {
        DebugTool {
            commands: Vec::new(),
        }
    }

    pub fn line(
        this: &Option<Instance<DebugTool, Shared>>,
        callback: Box<dyn Fn() -> (Vector3, Vector3) + Send + Sync>,
    ) {
        if !OS::godot_singleton().is_debug_build() {
            return;
        }

        if let Some(debug_tool) = this {
            unsafe { debug_tool.assume_safe() }
                .map_mut(|debug_tool, _| {
                    debug_tool.commands.push((
                        "begin".to_string(),
                        vec![Mesh::PRIMITIVE_LINES.to_variant()],
                    ));

                    let (from, to) = callback();

                    debug_tool
                        .commands
                        .push(("add_vertex".to_string(), vec![from.to_variant()]));

                    debug_tool
                        .commands
                        .push(("add_vertex".to_string(), vec![to.to_variant()]));

                    debug_tool.commands.push(("end".to_string(), Vec::new()));
                })
                .unwrap();
        }
    }
}

#[methods]
impl DebugTool {
    #[godot]
    fn _process(&mut self, #[base] owner: &ImmediateGeometry, _delta: f32) {
        owner.clear();

        for command in self.commands.iter() {
            unsafe { owner.call(&command.0, &command.1) };
        }

        self.commands.clear();
    }
}
