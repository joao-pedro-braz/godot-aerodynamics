use crate::Vector3;

const AIR_MOLAR_MASS: f32 = 0.02896968;
const GAS_CONSTANT: f32 = 8.314462618;
const SEA_LEVEL_DENSITY: f32 = 1.2250;

pub struct AerodynamicsState {
    pub barometric_pressure: f32,
    pub wind_velocity: Vector3,
    pub wind_velocity_normalized: Vector3,
    pub wind_speed_squared: f32,
}

impl AerodynamicsState {
    pub fn compute_earth_barometric_pressure_from_altitude(
        altitude: f32,
        gravity: f32,
        temperature: f32,
    ) -> f32 {
        SEA_LEVEL_DENSITY
            * ((-gravity * AIR_MOLAR_MASS * altitude) / (GAS_CONSTANT * temperature)).exp()
    }
}
