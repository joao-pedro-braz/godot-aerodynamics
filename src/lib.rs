use gdnative::prelude::*;

use crate::utils::debug_tool::DebugTool;

mod aerodynamic_body;
mod aerodynamic_shape;
mod aerodynamic_surface;
mod aerodynamics_state;
mod drag_coefficients;
mod shapes;
mod utils;

fn init(handle: InitHandle) {
    handle.add_tool_class::<aerodynamic_body::AerodynamicBody>();

    handle.add_tool_class::<DebugTool>()
}

godot_init!(init);
