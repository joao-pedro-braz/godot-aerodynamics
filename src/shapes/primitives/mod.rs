use gdnative::api::{CollisionShape, PhysicsDirectBodyState, RigidBody};
use gdnative::prelude::*;

use crate::aerodynamic_surface::AerodynamicSurface;
use crate::aerodynamics_state::AerodynamicsState;
use crate::DebugTool;

pub mod box_shape;
pub mod capsule_shape;
pub mod convex_polygon_shape;
pub mod cylinder_shape;
pub mod sphere_shape;

#[derive(Debug)]
pub struct AerodynamicPrimitiveShape {
    pub shape: Ref<CollisionShape>,
    pub surfaces: Vec<AerodynamicSurface>,
}

impl AerodynamicPrimitiveShape {
    pub fn apply_aerodynamics(
        &self,
        _owner: &RigidBody,
        physics_direct_body_state: TRef<PhysicsDirectBodyState>,
        aerodynamic_state: &AerodynamicsState,
        debug_tool: &Option<Instance<DebugTool, Shared>>,
    ) {
        let global_local_transform = (physics_direct_body_state.transform()
            * self.local_transform())
        .basis
        .orthonormalized();

        for surface in self.surfaces.iter() {
            let center = global_local_transform.xform(surface.center);

            let forward = if surface.infinitely_sided {
                -aerodynamic_state.wind_velocity.normalized()
            } else {
                global_local_transform.xform(surface.normal)
            };

            let magnitude = aerodynamic_state.wind_velocity_normalized.dot(forward);

            if surface.one_sided && magnitude <= 0.0 {
                continue;
            }

            let estimated_drag_coefficient = surface.drag_coefficient * magnitude;
            let drag_force = aerodynamic_state.barometric_pressure / 2.0
                * aerodynamic_state.wind_speed_squared
                * surface.area
                * estimated_drag_coefficient;

            let direction = -forward.normalized();

            if (direction * drag_force).length() > 0.0 {
                let global_force = direction * drag_force * physics_direct_body_state.step() as f32;

                // So we can safely capture our variables
                {
                    let surface_center = surface.center;
                    let global_force = global_force;
                    let local_transform = self.local_transform();
                    let global_local_transform = global_local_transform;
                    DebugTool::line(
                        debug_tool,
                        Box::new(move || {
                            let center = local_transform.xform(surface_center);
                            let force = local_transform
                                .basis
                                .xform(global_local_transform.xform_inv(global_force));

                            (center, center + force)
                        }),
                    );
                }

                physics_direct_body_state.add_force(global_force, center);
            }
        }
    }
}

impl AerodynamicPrimitiveShape {
    fn local_transform(&self) -> Transform {
        unsafe { self.shape.assume_safe() }.transform()
    }
}

impl AerodynamicPrimitiveShape {
    pub fn draw_debug(&self, _owner: &RigidBody, debug_tool: &Option<Instance<DebugTool>>) {
        for surface in self.surfaces.iter() {
            let center = self.local_transform().xform(surface.center);
            let normal = self.local_transform().basis.xform(surface.normal);

            DebugTool::line(
                debug_tool,
                Box::new(move || (center, center + normal * 2.0)),
            );
        }
    }
}

pub trait FromShape<Shape> {
    fn new(collision_shape: &CollisionShape, shape: &Shape) -> Self;
}
