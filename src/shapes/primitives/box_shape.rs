use gdnative::api::{BoxShape, CollisionShape};
use gdnative::prelude::*;

use crate::aerodynamic_surface::AerodynamicSurface;
use crate::drag_coefficients::RECTANGLE_DRAG_COEFFICIENT;
use crate::shapes::primitives::{AerodynamicPrimitiveShape, FromShape};

impl FromShape<BoxShape> for AerodynamicPrimitiveShape {
    fn new(collision_shape: &CollisionShape, box_shape: &BoxShape) -> AerodynamicPrimitiveShape {
        let extents = box_shape.extents();

        // We assume the box is axis aligned and we are looking at it down Z-
        // Order of surfaces is front, right, bottom, left, top and back
        let surface_one = AerodynamicSurface {
            area: extents.x * 2.0 * extents.y * 2.0,
            normal: Vector3::new(0.0, 0.0, 1.0),
            center: Vector3::new(0.0, 0.0, extents.z),
            one_sided: true,
            infinitely_sided: false,
            drag_coefficient: RECTANGLE_DRAG_COEFFICIENT,
        };
        let surface_two = AerodynamicSurface {
            area: extents.y * 2.0 * extents.z * 2.0,
            normal: Vector3::new(1.0, 0.0, 0.0),
            center: Vector3::new(extents.x, 0.0, 0.0),
            one_sided: true,
            infinitely_sided: false,
            drag_coefficient: RECTANGLE_DRAG_COEFFICIENT,
        };
        let surface_three = AerodynamicSurface {
            area: extents.x * 2.0 * extents.z * 2.0,
            normal: Vector3::new(0.0, -1.0, 0.0),
            center: Vector3::new(0.0, -extents.y, 0.0),
            one_sided: true,
            infinitely_sided: false,
            drag_coefficient: RECTANGLE_DRAG_COEFFICIENT,
        };
        let surface_four = AerodynamicSurface {
            area: extents.y * 2.0 * extents.z * 2.0,
            normal: Vector3::new(-1.0, 0.0, 0.0),
            center: Vector3::new(-extents.x, 0.0, 0.0),
            one_sided: true,
            infinitely_sided: false,
            drag_coefficient: RECTANGLE_DRAG_COEFFICIENT,
        };
        let surface_five = AerodynamicSurface {
            area: extents.x * 2.0 * extents.z * 2.0,
            normal: Vector3::new(0.0, 1.0, 0.0),
            center: Vector3::new(0.0, extents.y, 0.0),
            one_sided: true,
            infinitely_sided: false,
            drag_coefficient: RECTANGLE_DRAG_COEFFICIENT,
        };
        let surface_six = AerodynamicSurface {
            area: extents.x * 2.0 * extents.y * 2.0,
            normal: Vector3::new(0.0, 0.0, -1.0),
            center: Vector3::new(0.0, 0.0, -extents.z),
            one_sided: true,
            infinitely_sided: false,
            drag_coefficient: RECTANGLE_DRAG_COEFFICIENT,
        };

        AerodynamicPrimitiveShape {
            shape: unsafe { collision_shape.assume_shared() },
            surfaces: vec![
                surface_one,
                surface_two,
                surface_three,
                surface_four,
                surface_five,
                surface_six,
            ],
        }
    }
}
