use std::f64::consts::PI;

use gdnative::api::{CapsuleShape, CollisionShape};
use gdnative::prelude::*;

use crate::aerodynamic_surface::AerodynamicSurface;
use crate::drag_coefficients::{CYLINDER_DRAG_COEFFICIENT, HALF_SPHERE_DRAG_COEFFICIENT};
use crate::shapes::primitives::{AerodynamicPrimitiveShape, FromShape};

impl FromShape<CapsuleShape> for AerodynamicPrimitiveShape {
    fn new(
        collision_shape: &CollisionShape,
        capsule_shape: &CapsuleShape,
    ) -> AerodynamicPrimitiveShape {
        // We assume the capsule is lying down along the Z axis and we're looking at it
        // Down the Z- axis
        let left_right_sides = AerodynamicSurface {
            area: (PI * capsule_shape.radius() * capsule_shape.height()) as f32 / 2.0,
            normal: Vector3::new(1.0, 0.0, 0.0),
            center: Vector3::new(0.0, 0.0, 0.0),
            one_sided: false,
            infinitely_sided: false,
            drag_coefficient: CYLINDER_DRAG_COEFFICIENT,
        };
        let top_bottom_sides = AerodynamicSurface {
            area: (PI * capsule_shape.radius() * capsule_shape.height()) as f32 / 2.0,
            normal: Vector3::new(0.0, 1.0, 0.0),
            center: Vector3::new(0.0, 0.0, 0.0),
            one_sided: false,
            infinitely_sided: false,
            drag_coefficient: CYLINDER_DRAG_COEFFICIENT,
        };
        let front_side = AerodynamicSurface {
            area: (2.0 * PI * capsule_shape.radius() * capsule_shape.radius()) as f32,
            normal: Vector3::new(0.0, 0.0, 1.0),
            center: Vector3::new(0.0, 0.0, (capsule_shape.height() * 0.5) as f32),
            one_sided: true,
            infinitely_sided: false,
            drag_coefficient: HALF_SPHERE_DRAG_COEFFICIENT,
        };
        let back_side = AerodynamicSurface {
            area: (2.0 * PI * capsule_shape.radius() * capsule_shape.radius()) as f32,
            normal: Vector3::new(0.0, 0.0, -1.0),
            center: Vector3::new(0.0, 0.0, -(capsule_shape.height() * 0.5) as f32),
            one_sided: true,
            infinitely_sided: false,
            drag_coefficient: HALF_SPHERE_DRAG_COEFFICIENT,
        };

        AerodynamicPrimitiveShape {
            shape: unsafe { collision_shape.assume_shared() },
            surfaces: vec![left_right_sides, top_bottom_sides, front_side, back_side],
        }
    }
}
