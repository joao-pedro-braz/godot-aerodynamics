use gdnative::api::{CollisionShape, ConvexPolygonShape};
use gdnative::prelude::*;

use crate::aerodynamic_surface::AerodynamicSurface;
use crate::drag_coefficients::TRIANGLE_DRAG_COEFFICIENT;
use crate::shapes::primitives::{AerodynamicPrimitiveShape, FromShape};

impl FromShape<ConvexPolygonShape> for AerodynamicPrimitiveShape {
    fn new(
        collision_shape: &CollisionShape,
        convex_polygon_shape: &ConvexPolygonShape,
    ) -> AerodynamicPrimitiveShape {
        let points: Vec<_> = convex_polygon_shape
            .points()
            .to_vec()
            .iter()
            .map(|point| [point.x as f64, point.y as f64, point.z as f64])
            .collect();
        let convex_hull = qhull_rs::ConvexHull::from_arrays(&points);

        // Build the shape centroid as to correct any flipped normals
        let mut centroid: [f64; 3] = [0.0, 0.0, 0.0];
        let mut is_first = true;
        let mut vertices_amount = 0_f64;
        for vertex in convex_hull.vertices() {
            let (x, y, z) = (vertex.point()[0], vertex.point()[1], vertex.point()[2]);

            if is_first {
                centroid = [x, y, z];
                is_first = false;
            } else {
                centroid = [centroid[0] + x, centroid[1] + y, centroid[2] + z];
            }

            vertices_amount += 1.0;
        }

        let centroid = Vector3::new(
            (centroid[0] / vertices_amount) as f32,
            (centroid[1] / vertices_amount) as f32,
            (centroid[2] / vertices_amount) as f32,
        );

        AerodynamicPrimitiveShape {
            shape: unsafe { collision_shape.assume_shared() },
            surfaces: convex_hull
                .facets()
                .map(|facet| {
                    let vertices: Vec<_> = facet.vertices().map(|vertex| vertex.point()).collect();
                    let a = Vector3::new(
                        vertices[0][0] as f32,
                        vertices[0][1] as f32,
                        vertices[0][2] as f32,
                    );
                    let b = Vector3::new(
                        vertices[1][0] as f32,
                        vertices[1][1] as f32,
                        vertices[1][2] as f32,
                    );
                    let c = Vector3::new(
                        vertices[2][0] as f32,
                        vertices[2][1] as f32,
                        vertices[2][2] as f32,
                    );

                    let mut normal = (b - a).cross(c - a);
                    let area = normal.length() / 2.0;
                    let center = (a + b + c) / 3.0;

                    // If the normal is flipped, inverse it so it faces outside the shape
                    if normal.dot(center - centroid) < 0.0 {
                        normal = -normal;
                    }

                    AerodynamicSurface {
                        area,
                        center,
                        normal,
                        one_sided: true,
                        infinitely_sided: false,
                        drag_coefficient: TRIANGLE_DRAG_COEFFICIENT,
                    }
                })
                .collect(),
        }
    }
}
