use std::f64::consts::PI;

use gdnative::api::{CollisionShape, SphereShape};
use gdnative::prelude::*;

use crate::aerodynamic_surface::AerodynamicSurface;
use crate::drag_coefficients::SPHERE_DRAG_COEFFICIENT;
use crate::shapes::primitives::{AerodynamicPrimitiveShape, FromShape};

impl FromShape<SphereShape> for AerodynamicPrimitiveShape {
    fn new(
        collision_shape: &CollisionShape,
        sphere_shape: &SphereShape,
    ) -> AerodynamicPrimitiveShape {
        // Spheres have infinite sides, so we set infinitely_sided to true
        let all_sides = AerodynamicSurface {
            area: (4.0 * PI * sphere_shape.radius() * sphere_shape.radius()) as f32,
            normal: Vector3::new(0.0, 0.0, 0.0),
            center: Vector3::new(0.0, 0.0, 0.0),
            one_sided: false,
            infinitely_sided: true,
            drag_coefficient: SPHERE_DRAG_COEFFICIENT,
        };

        AerodynamicPrimitiveShape {
            shape: unsafe { collision_shape.assume_shared() },
            surfaces: vec![all_sides],
        }
    }
}
