use std::f64::consts::PI;

use gdnative::api::{CollisionShape, CylinderShape};
use gdnative::prelude::*;

use crate::aerodynamic_surface::AerodynamicSurface;
use crate::drag_coefficients::{CIRCLE_DRAG_COEFFICIENT, CYLINDER_DRAG_COEFFICIENT};
use crate::shapes::primitives::{AerodynamicPrimitiveShape, FromShape};

impl FromShape<CylinderShape> for AerodynamicPrimitiveShape {
    fn new(
        collision_shape: &CollisionShape,
        cylinder_shape: &CylinderShape,
    ) -> AerodynamicPrimitiveShape {
        // We assume the cylinder is upright the Y axis and we're looking at it
        // Along the Z+ axis
        let left_right_sides = AerodynamicSurface {
            area: (PI * cylinder_shape.radius() * cylinder_shape.height()) as f32 / 2.0,
            normal: Vector3::new(1.0, 0.0, 0.0),
            center: Vector3::new(0.0, 0.0, 0.0),
            one_sided: false,
            infinitely_sided: false,
            drag_coefficient: CYLINDER_DRAG_COEFFICIENT,
        };
        let front_back_sides = AerodynamicSurface {
            area: (PI * cylinder_shape.radius() * cylinder_shape.height()) as f32 / 2.0,
            normal: Vector3::new(0.0, 0.0, 1.0),
            center: Vector3::new(0.0, 0.0, 0.0),
            one_sided: false,
            infinitely_sided: false,
            drag_coefficient: CYLINDER_DRAG_COEFFICIENT,
        };
        let top_side = AerodynamicSurface {
            area: (PI * cylinder_shape.radius()) as f32,
            normal: Vector3::new(0.0, 1.0, 0.0),
            center: Vector3::new(0.0, (cylinder_shape.height() * 0.5) as f32, 0.0),
            one_sided: true,
            infinitely_sided: false,
            drag_coefficient: CIRCLE_DRAG_COEFFICIENT,
        };
        let bottom_side = AerodynamicSurface {
            area: (PI * cylinder_shape.radius()) as f32,
            normal: Vector3::new(0.0, -1.0, 0.0),
            center: Vector3::new(0.0, -(cylinder_shape.height() * 0.5) as f32, 0.0),
            one_sided: true,
            infinitely_sided: false,
            drag_coefficient: CIRCLE_DRAG_COEFFICIENT,
        };

        AerodynamicPrimitiveShape {
            shape: unsafe { collision_shape.assume_shared() },
            surfaces: vec![left_right_sides, front_back_sides, top_side, bottom_side],
        }
    }
}
