use gdnative::api::{
    BoxShape, CapsuleShape, CollisionShape, ConvexPolygonShape, CylinderShape,
    PhysicsDirectBodyState, ProjectSettings, RigidBody, SphereShape,
};
use gdnative::prelude::*;

use crate::aerodynamics_state::AerodynamicsState;
use crate::shapes::primitives::{AerodynamicPrimitiveShape, FromShape};
use crate::DebugTool;

#[derive(Debug)]
pub enum AerodynamicShape {
    Primitive(AerodynamicPrimitiveShape),
    None,
}

impl AerodynamicShape {
    pub fn from_collision_shape(collision_shape_ref: Ref<CollisionShape>) -> Self {
        let collision_shape = unsafe { collision_shape_ref.assume_safe() };
        let shape_ref = collision_shape.shape().unwrap();
        let shape = unsafe { shape_ref.assume_safe() };

        if let Some(box_shape) = shape.cast::<BoxShape>() {
            return AerodynamicShape::Primitive(AerodynamicPrimitiveShape::new(
                &collision_shape,
                &box_shape as &BoxShape,
            ));
        } else if let Some(capsule_shape) = shape.cast::<CapsuleShape>() {
            return AerodynamicShape::Primitive(AerodynamicPrimitiveShape::new(
                &collision_shape,
                &capsule_shape as &CapsuleShape,
            ));
        } else if let Some(cylinder_shape) = shape.cast::<CylinderShape>() {
            return AerodynamicShape::Primitive(AerodynamicPrimitiveShape::new(
                &collision_shape,
                &cylinder_shape as &CylinderShape,
            ));
        } else if let Some(sphere_shape) = shape.cast::<SphereShape>() {
            return AerodynamicShape::Primitive(AerodynamicPrimitiveShape::new(
                &collision_shape,
                &sphere_shape as &SphereShape,
            ));
        } else if let Some(convex_polygon_shape) = shape.cast::<ConvexPolygonShape>() {
            return AerodynamicShape::Primitive(AerodynamicPrimitiveShape::new(
                &collision_shape,
                &convex_polygon_shape as &ConvexPolygonShape,
            ));
        }

        AerodynamicShape::None
    }

    pub fn apply_aerodynamics(
        &self,
        owner: &RigidBody,
        physics_direct_body_state_ref: &Ref<PhysicsDirectBodyState>,
        debug_tool: &Option<Instance<DebugTool, Shared>>,
    ) {
        let physics_direct_body_state = unsafe { physics_direct_body_state_ref.assume_safe() };
        let aerodynamics_state = AerodynamicsState {
            barometric_pressure: AerodynamicsState::compute_earth_barometric_pressure_from_altitude(
                physics_direct_body_state.transform().origin.y,
                ProjectSettings::godot_singleton()
                    .get_setting("physics/3d/default_gravity")
                    .try_to::<f32>()
                    .unwrap(),
                288.15,
            ),
            wind_velocity: physics_direct_body_state.linear_velocity(),
            wind_velocity_normalized: physics_direct_body_state.linear_velocity().normalized(),
            wind_speed_squared: physics_direct_body_state.linear_velocity().length_squared(),
        };

        if aerodynamics_state.wind_velocity.length() == 0.0 {
            // Aerodynamics can only apply if the rigid-body is moving
            return;
        }

        match self {
            AerodynamicShape::Primitive(primitive_shape) => primitive_shape.apply_aerodynamics(
                owner,
                physics_direct_body_state,
                &aerodynamics_state,
                debug_tool,
            ),
            AerodynamicShape::None => {}
        };
    }
}

impl AerodynamicShape {
    pub fn draw_debug(&self, owner: &RigidBody, debug_tool: &Option<Instance<DebugTool>>) {
        match self {
            AerodynamicShape::Primitive(primitive_shape) => {
                primitive_shape.draw_debug(owner, debug_tool)
            }
            AerodynamicShape::None => {}
        };
    }
}
