use gdnative::prelude::*;

#[derive(Debug, Clone)]
pub struct AerodynamicSurface {
    pub center: Vector3,
    pub normal: Vector3,
    pub one_sided: bool,
    pub infinitely_sided: bool,
    pub drag_coefficient: f32,
    pub area: f32,
}
